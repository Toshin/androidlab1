/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


 <script src="https://www.gstatic.com/firebasejs/3.6.5/firebase.js"></script>

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBRsGZcpS4Y9j8iqagsvfhcRbYanazYcdY",
    authDomain: "onlinegamestorel7.firebaseapp.com",
    databaseURL: "https://onlinegamestorel7.firebaseio.com",
    storageBucket: "onlinegamestorel7.appspot.com",
    messagingSenderId: "420289961414"
  };
  Firebase.initializeApp(config);


var firebase = require("firebase/app");
require("firebase/auth");
require("firebase/database");


import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  ScrollView,
  Image,
  TextInput,
  Navigator,
  TouchableOpacity,
  Alert,
  TouchableHighlight,
  AsyncStorage,
} from 'react-native';

import Communications from 'react-native-communications';
import { BarChart } from 'react-native-charts'
import Notification from 'react-native-notification';
import Firebase from 'firebase';

var games = []


import Button from 'react-native-button';

var SCREEN_WIDTH = require('Dimensions').get('window').width;

var BaseConfig = Navigator.SceneConfigs.FloatFromRight;

var CustomLeftToRightGesture = Object.assign({}, BaseConfig.gestures.pop, {
  // Make it snap back really quickly after canceling pop
  snapVelocity: 8,
  // Make it so we can drag anywhere on the screen
  edgeHitWidth: SCREEN_WIDTH,
});

var CustomSceneConfig = Object.assign({}, BaseConfig, {
  // A very tighly wound spring will make this transition fast
  springTension: 100,
  springFriction: 1,
  // Use our custom gesture defined above
  gestures: {
    pop: CustomLeftToRightGesture,
  }
});


class MyScene extends React.Component {

	  constructor(props) {
    super(props);

	this.itemsRef = Firebase.database().ref();
    this.state = {
		role : props.role
    };
  }

	_handlePress() {

    Communications.email(["marianlaslo@yahoo.com"],"","","OGS - Your Order",
                 "You have ordered the following game: \n" +
                 "Game name: " + this.state.name +
                 "\nGame description: " + this.state.description +
                 "\nGame price: " + this.state.price +
                 "\n \nBest regards, \n"+
                 "OGS - Online Game Store.");


		this.itemsRef.push({ name: this.state.name, description: this.state.description, price : this.state.price, action : this.state.action,
		simulator : this.state.simulator, shooter : this.state.shooter, rpg : this.state.rpg});



		this.props.navigator.push({id :4,
			passProps : {
				role : this.state.role
			}
		});
  }



  render() {
    return (
    <View style={{padding: 10}}>
        <TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert game name"
		    ref= "name"
			onChangeText={(name) => this.setState({name})}
			value={this.state.name}
        />
		 <TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert game description"
			ref= "description"
			onChangeText={(description) => this.setState({description})}
			value={this.state.description}
        />
		 <TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert game price"
			ref= "price"
			onChangeText={(price) => this.setState({price})}
			value={this.state.price}
        />
		<TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert action %"
			ref= "action"
			onChangeText={(action) => this.setState({action})}
			value={this.state.action}
        />
		<TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert simulator %"
			ref= "simulator"
			onChangeText={(simulator) => this.setState({simulator})}
			value={this.state.simulator}
        />
		<TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert shooter %"
			ref= "shooter"
			onChangeText={(shooter) => this.setState({shooter})}
			value={this.state.shooter}
        />
		<TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert RPG %"
			ref= "rpg"
			onChangeText={(rpg) => this.setState({rpg})}
			value={this.state.rpg}
        />

		 <Button
       containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
  			style={{fontSize: 20, color: 'white'}}
  			styleDisabled={{color: 'red'}}
  			onPress={() => this._handlePress()}>
			Add Game
		</Button>

      </View>
    )
  }
}



class EditScene extends React.Component {

	constructor(props) {
    super(props);

	this.itemsRef = Firebase.database().ref();

    this.state = {
			description : props.game.description,
			price : props.game.price,
			role : props.role,
    };
  }


	_handlePress() {

		var a = this.state.description;
		var b = this.state.price;
		var query = this.itemsRef.orderByChild('name').equalTo(this.props.game.name);
				query.on('child_added', function(snapshot) {
					snapshot.ref.update({"description" : a, "price" : b});
				})



		this.props.navigator.push({id :4,
			passProps : {
				role : this.state.role
			}
		});
  }


		_handelFirebaseDelete(){


			var query = this.itemsRef.orderByChild('name').equalTo(this.props.game.name);
				query.on('child_added', function(snapshot) {
					snapshot.ref.remove();
				})


			this.props.navigator.push({id :4,
			passProps : {
				role : this.state.role
			}
		});
		}



	_renderEditButton(){
		if(this.state.role == 1){
			return(
				 <Button
		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
			style={{fontSize: 20, color: 'white'}}
			styleDisabled={{color: 'red'}}
			onPress={() => this._handlePress()}>
			Edit Game
		</Button>
			)
		}else{
			return null;
		}

	}

	_renderDeleteButton(){

			if(this.state.role == 1){
			return(
			 <Button
		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
			style={{fontSize: 20, color: 'white'}}
			styleDisabled={{color: 'red'}}
			onPress={() => this._handelFirebaseDelete()}>
			Delete Game
		</Button>
			)
		}else{
			return null;
		}
	}

  render() {

    return (
    <ScrollView style={{padding: 10}}>

		 <TextInput
			style={{height: 60}}
			ref= "description"
			onChangeText={(description) => this.setState({description})}
			value={this.state.description}
        />
		 <TextInput
			style={{height: 60}}
			ref= "price"
			onChangeText={(price) => this.setState({price})}
			value={this.state.price}
        />

		{this._renderEditButton()}

		<Text>
		</Text>

		{this._renderDeleteButton()}
		<BarChart
					  dataSets={[
						{
						  fillColor: '#46b3f7',
						  data: [
							{ value: this.props.game.action },
							{ value: this.props.game.simulator },
							{ value: this.props.game.shooter },
							{ value: this.props.game.rpg },
						  ]
						},

					  ]}
					  graduation={1}
					  horizontal={false}
					  showGrid={true}
					  barSpacing={5}
					  style={{
						height: 200,
						margin: 15,
				}}/>
			<Text>
        Chart legend:	{"\n"}
                Column 1: Action Genre %{"\n"}
								Column 2: Simulation Genre %{"\n"}
								Column 3: Shooter Genre %{"\n"}
								Column 4: RPG Genre %{"\n"}
								{"\n"}
								{"\n"}
								{"\n"}
			</Text>

      </ScrollView>
    )
  }
}


class V3ReactOGS extends Component {

	constructor(props){
		super(props);

		this.itemsRef = Firebase.database().ref();

		const ds = new ListView.DataSource({
			rowHasChanged: (row1, row2) => row1 !== row2,
		      });

		if(games.length == 0){
			this.listenForItems(this.itemsRef);
		}


		this.state={
		dataSource : ds.cloneWithRows(games),
		      loaded: true,
			 role : this.props.items

		}
	}





		listenForItems(itemsRef) {
			itemsRef.on('value', (snap) => {

			  // get children as an array

			  while(games.length > 0) {
				games.pop();
			}

			  snap.forEach((child) => {
				games.push({ "name": child.val().name, "description": child.val().description, "price" : child.val().price, "action" : child.val().action,
						"simulator" : child.val().simulator, "shooter" : child.val().shooter , "rpg" : child.val().rpg});


			});

			 this.setState({
                             dataSource: this.state.dataSource.cloneWithRows(games),
                             loaded: true,
                         });
			  });
		  }



	_onPressButton(game) {

		this.props.navigator.push({id: 3,
			passProps:{
				game : game,
				role : this.state.role,
				k : game._key,

			}
		});



	}


	componentDidMont(){


		this.setState({
			dataSource: ds.cloneWithRows(games),
			loaded: true,
		});

	}




	renderGame(game){

		return (
		<TouchableOpacity onPress={this._onPressButton.bind(this,game)}>
			<View
			style={styles.viewDetails}>
				<Text>{game.name}</Text>
				<Text>{game.description}</Text>
				<Text>{game.price}</Text>
			</View>
		</TouchableOpacity>
			);
	}


 render(){
	 if (!this.state.loaded){
		 return (<Text> Please wait.. </Text>);
	 }

	 return(
		<ListView
			dataSource={this.state.dataSource}
			renderRow={this.renderGame.bind(this)}
			style={styles.listView}
		/>
	 );

	}

}


class App extends Component {

	constructor(props){
		super(props);
			this.state = {
		role : props.role
    };
	}

	_renderAddButton(){
		if(this.state.role == 1){
			return(
					<Button
				 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
					style={{fontSize: 20, color: 'white'}}
					styleDisabled={{color: 'red'}}
					onPress={() => this._handlePress()}>
					Add Game
				</Button>
			)
		}else{
			return null;
		}
	}

	_handlePress() {
		this.props.navigator.push({id: 2,
			passProps :{
				role : this.state.role
			}
		});
	}


  render() {
    return (

      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to Online Game Store
        </Text>

		{this._renderAddButton()}
		<V3ReactOGS navigator={this.props.navigator} items={this.state.role}/>

      </View>
    );
  }
}


class LoginOrRegister extends Component {
	constructor(props){
		super(props);
	}

	_handleLoginPress(){

		this.props.navigator.push({id: 5,});
	}

	_handleRegisterPress(){
		this.props.navigator.push({id: 6,});
	}

	render (){
		return(
			<View>

				<Text style={styles.welcome}>
					Welcome to Online Game Store
				</Text>

				 <Button
					containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
					style={{fontSize: 20, color: 'white'}}
					styleDisabled={{color: 'red'}}
					onPress={() => this._handleLoginPress()}>
					Login
				</Button>
				<Text>
				</Text>
				 <Button
					containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
					style={{fontSize: 20, color: 'white'}}
					styleDisabled={{color: 'red'}}
					onPress={() => this._handleRegisterPress()}>
					Register
				</Button>

			</View>
		)
	}

}


class FirebaseSignUp extends Component{
	constructor(props){
		super(props);


		this.state = {
			loaded: true,
			email: '',
			password: ''
		};
	}

	_signup(){

		this.setState({
		  loaded: false
		});


			firebase.auth().createUserWithEmailAndPassword(this.state.email,this.state.password)
				.then((user) => {
					this._goToLogin();
				}).catch(function(error){
					 var errorCode = error.code;
						var errorMessage = error.message;

						if (errorCode === 'auth/wrong-password') {
							alert('Wrong password.');
						} else {
							alert(errorMessage);
						}
						console.log(error);
				});


  }



  _goToLogin(){
     this.props.navigator.push({id: 5,});
  }

    render() {
    return (
      <View>


            <TextInput
                style={styles.textinput}
                onChangeText={(text) => this.setState({email: text})}
                value={this.state.email}
            placeholder={"Email Address"}
            />
          <TextInput
            style={styles.textinput}
            onChangeText={(text) => this.setState({password: text})}
            value={this.state.password}
            secureTextEntry={true}
            placeholder={"Password"}
          />


		  	 <Button
					containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
					style={{fontSize: 20, color: 'white'}}
					styleDisabled={{color: 'red'}}
					onPress={() => this._signup()}>
					Signup
				</Button>
				<Text>
				</Text>
		 <Button
					containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
					style={{fontSize: 20, color: 'white'}}
					styleDisabled={{color: 'red'}}
					onPress={() => this._goToLogin()}>
					Already registered?
				</Button>

      </View>
    );
  }

}


class FirebaseLogin extends Component{
	  constructor(props){
    super(props);

    this.state = {
      email: '',
      password: '',
      loaded: true
    }
  }


	  _login(){

    this.setState({
      loaded: false
    });


		firebase.auth().signInWithEmailAndPassword(this.state.email,this.state.password)
			.then((user) => {

				if(this.state.email.includes("admin")){
						this.props.navigator.push({id: 4,
						passProps:{
						role : 1,

					}});
				}else{
					this.props.navigator.push({id:4});
				}


			}).catch(function(error){
				 var errorCode = error.code;
					var errorMessage = error.message;

					if (errorCode === 'auth/wrong-password') {
						alert('Wrong password.');
					} else {
						alert(errorMessage);
					}
					console.log(error);
			});





  }
    _goToSignup(){
    this.props.navigator.push({id: 6,});
  }

   render(){
    return (
      <View>

          <TextInput
            style={styles.textinput}
            onChangeText={(text) => this.setState({email: text})}
            value={this.state.email}
            placeholder={"Email Address"}
          />
          <TextInput
            style={styles.textinput}
            onChangeText={(text) => this.setState({password: text})}
            value={this.state.password}
            secureTextEntry={true}
            placeholder={"Password"}
          />

			 <Button
					containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
					style={{fontSize: 20, color: 'white'}}
					styleDisabled={{color: 'red'}}
					onPress={() => this._login()}>
					Login
				</Button>
				<Text>
				</Text>
		 <Button
					containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
					style={{fontSize: 20, color: 'white'}}
					styleDisabled={{color: 'red'}}
					onPress={() => this._goToSignup()}>
					Don't have an account?
				</Button>


      </View>
    );
  }
}



var SampleApp = React.createClass({
  _renderScene(route, navigator) {
    if (route.id === 1) {
      return <LoginOrRegister navigator={navigator} {...route.passProps}/>
    } else if (route.id === 2) {
      return <MyScene navigator={navigator} {...route.passProps}/>
    } else if (route.id === 3){
		return <EditScene navigator={navigator} {...route.passProps} />
	} else if (route.id === 4){
		return <App navigator={navigator} {...route.passProps}/>
	} else if (route.id === 5){
		return <FirebaseLogin navigator={navigator} {...route.passProps} />
	} else if (route.id === 6){
		return <FirebaseSignUp navigator={navigator} {...route.passProps} />
	}
  },
  _configureScene(route) {
    return CustomSceneConfig;
  },



  render() {
    return (
      <Navigator
        initialRoute={{id: 1, }}
        renderScene={this._renderScene}
        configureScene={this._configureScene} />
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
listView: {
	width: 300,
    paddingTop: 20,
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  viewDetails: {
	  margin: 5
  },
});

AppRegistry.registerComponent('V3ReactOGS', () => SampleApp);
