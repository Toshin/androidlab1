package com.example.toshin.onlinegamestore;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class RegisterOrLogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_or_login);
    }

    public void btnRegistration_Click(View v){

        Intent reg = new Intent(RegisterOrLogin.this, RegistrationActivity.class);
        startActivity(reg);
    }

    public void btnLogin_Click(View v){
        Intent login = new Intent(RegisterOrLogin.this,LoginFirebaseActivty.class);
        startActivity(login);
    }

}
