package com.example.toshin.onlinegamestore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.toshin.onlinegamestore.dummy.DummyContent;

import java.util.List;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import static com.example.toshin.onlinegamestore.LoginFirebaseActivty.userRole;


/**
 * An activity representing a list of Games. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link GameDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class GameListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private DatabaseReference databaseReference;
    private DatabaseReference gamesReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_list);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        gamesReference = databaseReference.child("games");

        try{
            gamesReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    DummyContent.ITEMS.clear();

                    for (DataSnapshot game : dataSnapshot.getChildren()) {

                        String id = game.child("id").getValue().toString();
                        String title = game.child("title").getValue().toString();
                        String description = game.child("description").getValue().toString();
                        long price = (long)game.child("price").getValue();
                        long action = (long)game.child("action").getValue();
                        long simulator = (long)game.child("simulator").getValue();
                        long shooter = (long)game.child("shooter").getValue();
                        long rpg = (long)game.child("rpg").getValue();
                        DummyContent.Game g = new DummyContent.Game(id,title,description,price,action,simulator,shooter,rpg);

                        DummyContent.addItem(g);
                    }
                    View recyclerView = findViewById(R.id.game_list);
                    assert recyclerView != null;
                    setupRecyclerView((RecyclerView) recyclerView);
                }


                @Override
                public void onCancelled(DatabaseError firebaseError) {
                    Toast.makeText(GameListActivity.this, firebaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        FloatingActionButton logOut = (FloatingActionButton) findViewById(R.id.btnLogOut);

        if (userRole == 0) {
            fab.setVisibility(View.GONE);
        }

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(GameListActivity.this,RegisterOrLogin.class);
                startActivity(i);
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(GameListActivity.this, AddActivity.class);
                startActivity(i);
            }
        });



        if (findViewById(R.id.game_detail_container) != null) {
            mTwoPane = true;
        }
    }


    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(DummyContent.ITEMS));
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<DummyContent.Game> mValues;

        public SimpleItemRecyclerViewAdapter(List<DummyContent.Game> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.game_list_content, parent, false);
            return new ViewHolder(view);
        }


        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(mValues.get(position).id);
            holder.mContentView.setText(mValues.get(position).title);

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(GameDetailFragment.ARG_ITEM_ID, holder.mItem.id);
                        GameDetailFragment fragment = new GameDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.game_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, GameDetailActivity.class);
                        intent.putExtra(GameDetailFragment.ARG_ITEM_ID, holder.mItem.id);

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            public DummyContent.Game mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }
}
