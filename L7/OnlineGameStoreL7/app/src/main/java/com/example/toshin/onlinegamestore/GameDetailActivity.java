package com.example.toshin.onlinegamestore;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.toshin.onlinegamestore.dummy.DummyContent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.toshin.onlinegamestore.LoginFirebaseActivty.userRole;


/**
 * An activity representing a single Game detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link GameListActivity}.
 */
public class GameDetailActivity extends AppCompatActivity {

    private DatabaseReference databaseReference;
    private DatabaseReference gamesReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);


        databaseReference = FirebaseDatabase.getInstance().getReference();



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = getIntent().getStringExtra(GameDetailFragment.ARG_ITEM_ID);
                Intent intent = new Intent(GameDetailActivity.this, EditActivity.class);
                intent.putExtra("id",s);
                startActivity(intent);

                Snackbar.make(view, "edit pushed id = " + s, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        FloatingActionButton deletefab = (FloatingActionButton) findViewById(R.id.deletefab);

        if (userRole == 0) {
            fab.setVisibility(View.GONE);
            deletefab.setVisibility(View.GONE);
        }

        deletefab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String s = getIntent().getStringExtra(GameDetailFragment.ARG_ITEM_ID);
                final Query applesQuery = databaseReference.child("games").orderByChild("id").equalTo(s);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(GameDetailActivity.this);
                builder1.setMessage("Are you sure you want to delete this item?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {

                                                            applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                                    for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                                                        appleSnapshot.getRef().removeValue();
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {
                                                                    Toast.makeText(GameDetailActivity.this, "Error at delete", Toast.LENGTH_LONG).show();
                                                                }
                                                            });


                                                            Toast toast = Toast.makeText(GameDetailActivity.this,"Game deleted ", Toast.LENGTH_SHORT);
                                                            toast.show();

                                                            Intent intent = new Intent(GameDetailActivity.this, GameListActivity.class);
                                                            startActivity(intent);
                                                                            }
                                                    });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast toastn = Toast.makeText(GameDetailActivity.this,"Nothing deleted ", Toast.LENGTH_SHORT);
                                toastn.show();

                                Intent intent = new Intent(GameDetailActivity.this, GameListActivity.class);
                                startActivity(intent);

                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });

        FloatingActionButton chartButton = (FloatingActionButton) findViewById(R.id.chartBtn);
        chartButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String s = getIntent().getStringExtra(GameDetailFragment.ARG_ITEM_ID);

                ArrayList<Long> geners = new ArrayList<Long>();
                for(DummyContent.Game a: DummyContent.ITEMS){
                    if(a.id.equalsIgnoreCase(s)){
                        geners.add(a.action);
                        geners.add(a.simulator);
                        geners.add(a.shooter);
                        geners.add(a.rpg);

                    }
                }

                Intent intent = new Intent(getBaseContext(), ChartSecondActivity.class);
                intent.putExtra("genersValues", geners);
                startActivity(intent);

                Toast toast = Toast.makeText(GameDetailActivity.this,"Game chart ", Toast.LENGTH_SHORT);
                toast.show();

//                Intent intent = new Intent(GameDetailActivity.this, ChartSecondActivity.class);
//                startActivity(intent);
            }
        });


        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(GameDetailFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(GameDetailFragment.ARG_ITEM_ID));
            GameDetailFragment fragment = new GameDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.game_detail_container, fragment)
                    .commit();
        }
    }

    public void createFile() throws IOException, JSONException {

        JSONArray data = new JSONArray();
        JSONObject game;


        for(int i =0 ; i< DummyContent.ITEMS.size(); i++){
            game = new JSONObject();
            game.put("id",DummyContent.ITEMS.get(i).id);
            game.put("title",DummyContent.ITEMS.get(i).title);
            game.put("description",DummyContent.ITEMS.get(i).description);
            game.put("price",DummyContent.ITEMS.get(i).price);
            game.put("action",DummyContent.ITEMS.get(i).action);
            game.put("simulator",DummyContent.ITEMS.get(i).simulator);
            game.put("shooter",DummyContent.ITEMS.get(i).shooter);
            game.put("rpg",DummyContent.ITEMS.get(i).rpg);
            data.put(game);
        }

        String text = data.toString();

        FileOutputStream fos = openFileOutput("gamesFile", MODE_PRIVATE);
        fos.write(text.getBytes());
        fos.close();

        Toast toast = Toast.makeText(GameDetailActivity.this,"Data saved", Toast.LENGTH_SHORT);
        toast.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            navigateUpTo(new Intent(this, GameListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
