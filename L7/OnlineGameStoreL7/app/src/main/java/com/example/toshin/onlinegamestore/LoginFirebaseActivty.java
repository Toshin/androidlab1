package com.example.toshin.onlinegamestore;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginFirebaseActivty extends AppCompatActivity {

    public static int userRole = 0;

    private EditText txtEmailLogin;
    private EditText txtPasswordLogin;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_firebase_activty);

        txtEmailLogin = (EditText) findViewById(R.id.txtEmailLogin);
        txtPasswordLogin = (EditText) findViewById(R.id.txtPasswordLogin);
        firebaseAuth = FirebaseAuth.getInstance();

    }

    public void btnLoginUser_Click(View v){



        final ProgressDialog progressDialog = ProgressDialog.show(LoginFirebaseActivty.this, "Please wait...", "Proccessing...", true);
        (firebaseAuth.signInWithEmailAndPassword(txtEmailLogin.getText().toString(),txtPasswordLogin.getText().toString()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressDialog.dismiss();

                if(task.isSuccessful()){
                    String s = txtEmailLogin.getText().toString();
                    Toast.makeText(LoginFirebaseActivty.this, "Login successfull", Toast.LENGTH_LONG).show();
                    if(s.contains("admin")){
                        userRole = 1;
                    }

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(LoginFirebaseActivty.this)
                                    .setSmallIcon(R.drawable.notification_icon)
                                    .setContentTitle("Online Game Store notification")
                                    .setContentText("Welcome " + s);


                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(001,mBuilder.build());

                    Intent i = new Intent(LoginFirebaseActivty.this, GameListActivity.class);
                    startActivity(i);
                }else{
                    Log.e("Error",task.getException().toString());
                    Toast.makeText(LoginFirebaseActivty.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });
    }

}
