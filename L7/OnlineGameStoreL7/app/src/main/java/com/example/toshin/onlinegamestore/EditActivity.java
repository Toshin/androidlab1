package com.example.toshin.onlinegamestore;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.toshin.onlinegamestore.dummy.DummyContent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {


    private DatabaseReference databaseReference;
    private DatabaseReference gamesReference;
    private TextView mTextView;
    String takenId;
    DummyContent.Game e;
    DummyContent.Game edited;

    EditText id;
    EditText title;
    EditText description;
    EditText price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("id");
            takenId  = value;
        }

        Button editButton = (Button)findViewById(R.id.edit);
        editButton.setOnClickListener(this);


        title   = (EditText)findViewById(R.id.edittitle);
        description = (EditText)findViewById(R.id.editdescription);
        price = (EditText)findViewById(R.id.editprice);

        for (DummyContent.Game p : DummyContent.ITEMS) {
            if (p.id.equals(takenId)) {
                e = p;
                break;
            }
        }

        title.setText(e.title);
        description.setText(e.description);
        price.setText(String.valueOf(e.price));

    }

    public void createFile() throws IOException, JSONException {

        JSONArray data = new JSONArray();
        JSONObject game;


        for(int i =0 ; i< DummyContent.ITEMS.size(); i++){
            game = new JSONObject();
            game.put("id",DummyContent.ITEMS.get(i).id);
            game.put("title",DummyContent.ITEMS.get(i).title);
            game.put("description",DummyContent.ITEMS.get(i).description);
            game.put("price",DummyContent.ITEMS.get(i).price);
            game.put("action",DummyContent.ITEMS.get(i).action);
            game.put("simulator",DummyContent.ITEMS.get(i).simulator);
            game.put("shooter",DummyContent.ITEMS.get(i).shooter);
            game.put("rpg",DummyContent.ITEMS.get(i).rpg);
            data.put(game);
        }

        String text = data.toString();

        FileOutputStream fos = openFileOutput("gamesFile", MODE_PRIVATE);
        fos.write(text.getBytes());
        fos.close();

        Toast toast = Toast.makeText(EditActivity.this,"Data saved", Toast.LENGTH_SHORT);
        toast.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit: {


                final String t = EditActivity.this.title.getText().toString();
                final String d = EditActivity.this.description.getText().toString();
                Integer pr = Integer.parseInt(EditActivity.this.price.getText().toString());
                final Long lprice = (long)pr;

                AlertDialog.Builder builder1 = new AlertDialog.Builder(EditActivity.this);
                builder1.setMessage("Are you sure you want to edit this item?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                databaseReference = FirebaseDatabase.getInstance().getReference();
                                gamesReference = databaseReference.child("games");
                                Query pendingTasks = gamesReference.orderByChild("id").equalTo(takenId);
                                pendingTasks.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot tasksSnapshot) {
                                        for (DataSnapshot snapshot: tasksSnapshot.getChildren()) {
                                            snapshot.getRef().child("title").setValue(t);
                                            snapshot.getRef().child("description").setValue(d);
                                            snapshot.getRef().child("price").setValue(lprice);
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(EditActivity.this, "Error at edit", Toast.LENGTH_LONG).show();
                                    }
                                });


                                Toast toast = Toast.makeText(EditActivity.this,"Game edited ", Toast.LENGTH_SHORT);
                                toast.show();

                                Intent intent = new Intent(EditActivity.this, GameListActivity.class);
                                startActivity(intent);
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Intent intent = new Intent(EditActivity.this, GameListActivity.class);
                                startActivity(intent);
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();




            }
            }

        };


}
