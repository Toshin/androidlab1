package com.example.toshin.onlinegamestore.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Game> ITEMS = new ArrayList<Game>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, Game> ITEM_MAP = new HashMap<String, Game>();

    public static void addItem(Game item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class Game {
        public String id;
        public String title;
        public String description;
        public long price;
        public long action;
        public long simulator;
        public long shooter;
        public long rpg;

        public Game(){}


        public Game(String id, String t, String d,long i,long a,long b, long c, long r) {
            this.id = id;
            this.title = t;
            this.price = i;
            this.description = d;
            this.action =a;
            this.simulator = b;
            this.shooter = c;
            this.rpg = r;
        }

        public String getId(){
            return this.id;
        }


        @Override
        public String toString() {
            return "Game title: " + title + "\n" +
                    "Description: " + description + "\n" +
                    "Price: " + price + "$";
        }


    }
}
