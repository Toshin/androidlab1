/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  ScrollView,
  Image,
  TextInput,
  Navigator,
  TouchableOpacity,
  Alert,
  TouchableHighlight,
  AsyncStorage,
} from 'react-native';

import Communications from 'react-native-communications';
import { BarChart } from 'react-native-charts'


var asd = [
  { "name": "Battlefield 3", "description": "Description 1", "price" : "100"},
  { "name": "Battlefield 4", "description": "Description 2", "price" : "200"},
  { "name": "World of Warcraft", "description": "Description 3" , "price" : "500"},
  { "name": "World of Tanks", "description": "Description 4", "price" : "350"},
  { "name": "Counter-Strike GO", "description": "Description 5", "price" : "100"}
  ]

var games = []


import Button from 'react-native-button';

var SCREEN_WIDTH = require('Dimensions').get('window').width;

var BaseConfig = Navigator.SceneConfigs.FloatFromRight;

var CustomLeftToRightGesture = Object.assign({}, BaseConfig.gestures.pop, {
  // Make it snap back really quickly after canceling pop
  snapVelocity: 8,
  // Make it so we can drag anywhere on the screen
  edgeHitWidth: SCREEN_WIDTH,
});

var CustomSceneConfig = Object.assign({}, BaseConfig, {
  // A very tighly wound spring will make this transition fast
  springTension: 100,
  springFriction: 1,
  // Use our custom gesture defined above
  gestures: {
    pop: CustomLeftToRightGesture,
  }
});


class MyScene extends React.Component {

	  constructor(props) {
    super(props);
    this.state = {

    };
  }

	_handlePress() {

		games.push({ "name": this.state.name, "description": this.state.description, "price" : this.state.price, "action" : this.state.action,
		"simulation" : this.state.simulation, "shooter" : this.state.shooter, "rpg" : this.state.rpg});

    Communications.email(["marianlaslo@yahoo.com"],"","","OGS - Your Order",
                     "You have ordered the following game: \n" +
                     "Game name: " + this.state.name +
                     "\nGame description: " + this.state.description +
                     "\nGame price: " + this.state.price +
                     "\n \nBest regards, \n"+
                     "OGS - Online Game Store.");

		this._persistData();

		this.props.navigator.push({id :1});
  }


	_persistData(){
        return AsyncStorage.setItem('savedList', JSON.stringify(games))
                            .then(json => console.log('success! at persist save'))
                            .catch(error => console.log('error! at persist save'));
	}

  render() {
    return (
    <View style={{padding: 10}}>
        <TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert game name"
		    ref= "name"
			onChangeText={(name) => this.setState({name})}
			value={this.state.name}
        />
		 <TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert game description"
			ref= "description"
			onChangeText={(description) => this.setState({description})}
			value={this.state.description}
        />
		 <TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert game price"
			ref= "price"
			onChangeText={(price) => this.setState({price})}
			value={this.state.price}
        />
		<TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert action %"
			ref= "action"
			onChangeText={(action) => this.setState({action})}
			value={this.state.action}
        />
		<TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert simulation %"
			ref= "simulation"
			onChangeText={(simulation) => this.setState({simulation})}
			value={this.state.simulation}
        />
		<TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert shooter %"
			ref= "shooter"
			onChangeText={(shooter) => this.setState({shooter})}
			value={this.state.shooter}
        />
		<TextInput
			style={{height: 60, margin: 10, borderWidth: 1, borderColor: 'orange', borderStyle: 'dashed'}}
			placeholder="Insert RPG %"
			ref= "rpg"
			onChangeText={(rpg) => this.setState({rpg})}
			value={this.state.rpg}
        />

		 <Button
		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
			style={{fontSize: 20, color: 'white'}}
			styleDisabled={{color: 'red'}}
			onPress={() => this._handlePress()}>
			Add Game
		</Button>

      </View>
    )
  }
}



class EditScene extends React.Component {

	constructor(props) {
    super(props);
    this.state = {
			name : props.game.name,
			description : props.game.description,
			price : props.game.price,
    };
  }

	_persistData(){
        return AsyncStorage.setItem('savedList', JSON.stringify(games))
                            .then(json => console.log('success! at persist save'))
                            .catch(error => console.log('error! at persist save'));
	}

	_handlePress() {
		this.props.game.name = this.state.name;
		this.props.game.description = this.state.description;
		this.props.game.price = this.state.price;

		this._persistData();
		this.props.navigator.push({id :1});
  }

	 _handlePressDelete(){
      var index = games.indexOf(this.props.game);
      console.log("index = " + index);

      console.log("current length of array: " + games.length)
      if (index > -1) {
          games.splice(index, 1);
          console.log("new length of array: " + games.length)
          Alert.alert("Done","Deleted");

          this._persistData();
        }
        else{
          Alert.alert("Warning","Request not found. Can't delete");
        }
		this.props.navigator.push({id :1});
}


  render() {

    return (
    <ScrollView style={{padding: 10}}>
        <TextInput
			style={{height: 60}}
		    ref= "name"
			onChangeText={(name) => this.setState({name})}
			value={this.state.name}
        />
		 <TextInput
			style={{height: 60}}
			ref= "description"
			onChangeText={(description) => this.setState({description})}
			value={this.state.description}
        />
		 <TextInput
			style={{height: 60}}
			ref= "price"
			onChangeText={(price) => this.setState({price})}
			value={this.state.price}
        />

		 <Button
		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
			style={{fontSize: 20, color: 'white'}}
			styleDisabled={{color: 'red'}}
			onPress={() => this._handlePress()}>
			Edit Game
		</Button>
		<Text>
		</Text>
		 <Button
		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
			style={{fontSize: 20, color: 'white'}}
			styleDisabled={{color: 'red'}}
			onPress={() => this._handlePressDelete()}>
			Delete Game
		</Button>

		<BarChart
					  dataSets={[
						{
						  fillColor: 'orange',
						  data: [
							{ value: this.props.game.action },
							{ value: this.props.game.simulation },
							{ value: this.props.game.shooter },
							{ value: this.props.game.rpg },
						  ]
						},

					  ]}
					  graduation={1}
					  horizontal={false}
					  showGrid={true}
					  barSpacing={8}
					  style={{
						height: 200,
						margin: 15,
				}}/>
			<Text>
				Chart legend:	{"\n"}
                Column 1: Action Genre %{"\n"}
								Column 2: Simulation Genre %{"\n"}
								Column 3: Shooter Genre %{"\n"}
								Column 4: RPG Genre %{"\n"}
								{"\n"}
								{"\n"}
								{"\n"}
			</Text>

      </ScrollView>
    )
  }
}


class V2ReactOGS extends Component {

	constructor(props){
		super(props);

		const ds = new ListView.DataSource({
			rowHasChanged: (row1, row2) => row1 !== row2,
		      });

		if(games.length == 0){
			this._getPersistedData();
		}


		this.state={
		dataSource : ds.cloneWithRows(games),
		      loaded: true,
		}
	}



	 _getPersistedData(){
        return AsyncStorage.getItem('savedList')
            .then(g => JSON.parse(g))
            .then(json => {





                        for (var i = 0; i < json.length; i++) {
                        games.push({ "name": json[i].name, "description": json[i].description, "price" : json[i].price, "action" : json[i].action,
						"simulation" : json[i].simulation, "shooter" : json[i].shooter , "rpg" : json[i].rpg});



                        this.setState({
                             dataSource: this.state.dataSource.cloneWithRows(games),
                             loaded: true,
                         });
                    }
            })
            .catch(error => console.log('error! la cititre !!!!!'));

}

	_onPressButton(game) {

		this.props.navigator.push({id: 3,
			passProps:{
				game : game,

			}
		});



	}


	componentDidMont(){

	 for (var i = 0; i < games.length; i++) {
                       games.splice(i, 1);
                       }
		this._getPersistedData();

		this.setState({
			dataSource: ds.cloneWithRows(games),
			loaded: true,
		})

	}




	renderGame(game){

		return (
		<TouchableOpacity onPress={this._onPressButton.bind(this,game)}>
			<View
			style={styles.viewDetails}>
				<Text>{game.name}</Text>
				<Text>{game.description}</Text>
				<Text>{game.price}</Text>
			</View>
		</TouchableOpacity>
			);
	}


 render(){
	 if (!this.state.loaded){
		 return (<Text> Please wait!! </Text>);
	 }

	 return(
		<ListView
			dataSource={this.state.dataSource}
			renderRow={this.renderGame.bind(this)}
			style={styles.listView}
		/>
	 );

	}

}


class App extends Component {

	constructor(props){
		super(props);
	}

	_handlePress() {
		this.props.navigator.push({id: 2,});
	}


  render() {
    return (

      <View style={styles.container}>
        <Text style={styles.welcome}>
          Online Game Store
        </Text>

		 <Button
		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
			style={{fontSize: 20, color: 'white'}}
			styleDisabled={{color: 'red'}}
			onPress={() => this._handlePress()}>
			Add Game
    	</Button>
		<V2ReactOGS navigator={this.props.navigator} />

      </View>
    );
  }
}

var SampleApp = React.createClass({
  _renderScene(route, navigator) {
    if (route.id === 1) {
      return <App navigator={navigator} />
    } else if (route.id === 2) {
      return <MyScene navigator={navigator} />
    } else if (route.id === 3){
		return <EditScene navigator={navigator} {...route.passProps} />
	}
  },
  _configureScene(route) {
    return CustomSceneConfig;
  },



  render() {
    return (
      <Navigator
        initialRoute={{id: 1, }}
        renderScene={this._renderScene}
        configureScene={this._configureScene} />
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
listView: {
	width: 300,
    paddingTop: 20,
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  viewDetails: {
	  margin: 5
  },
});

AppRegistry.registerComponent('V2ReactOGS', () => SampleApp);
