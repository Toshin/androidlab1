package com.example.toshin.testingstuff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    HashMap<String, String> itemsDict = populateDictionary();
    String selectedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public HashMap<String, String> populateDictionary(){
        HashMap<String, String> itemsDict = new HashMap<>();
        itemsDict.put("bf3","Battlefield 3,0,EA Games" +
                ",0,03,09,2012");
        itemsDict.put("bf4","Battlefield 4,0,EA Games" +
                ",1,15,07,2014");
        itemsDict.put("worldofw","World of Warcraft,1,Blizzard" +
                ",0,09,11,2005");
        itemsDict.put("wot","World Of Tanks,2,Wargaming" +
                ",2,25,04,2010");
        itemsDict.put("csgo","Counter-Strige GO,0,Valve Corporation" +
                ",0,10,08,2012");
        return itemsDict;
    }

    public void fireInputActivity(View view){
        setContentView(R.layout.activity_input);
    }

    public void listItems(View view){
        setContentView(R.layout.activity_listitems);
    }

    public void setSelectedItem( View v ) {
        List<String> properties = new ArrayList<String>();
        String item;
        switch (v.getId()) {
            case (R.id.bf3):
                //stuff
                item = itemsDict.get("bf3");
                selectedId = "bf3";
                properties = Arrays.asList(item.split(","));
                break;
            case (R.id.bf4):
                //stuff
                item = itemsDict.get("bf4");
                selectedId = "bf4";
                properties = Arrays.asList(item.split(","));
                break;
            case (R.id.worldofw):
                //stuff
                item = itemsDict.get("worldofw");
                selectedId = "worldofw";
                properties = Arrays.asList(item.split(","));
            case (R.id.wot):
                //stuff
                item = itemsDict.get("wot");
                selectedId = "wot";
                properties = Arrays.asList(item.split(","));
                break;
            case (R.id.csgo):
                //stuff
                item = itemsDict.get("csgo");
                selectedId = "csgo";
                properties = Arrays.asList(item.split(","));
                break;
        }
        setContentView(R.layout.activity_item);
        EditText game = (EditText)findViewById(R.id.gameName);
        Spinner genre = (Spinner)findViewById(R.id.genreSpinner);
        EditText publisher   = (EditText)findViewById(R.id.publisherName);
        Spinner platform = (Spinner)findViewById(R.id.platformSpinner);
        DatePicker releaseDate = (DatePicker)findViewById(R.id.releaseDatePicker);
        game.setText(properties.get(0));
        genre.setSelection(Integer.parseInt(properties.get(1)));
        publisher.setText(properties.get(2));
        platform.setSelection(Integer.parseInt(properties.get(3)));
        releaseDate.updateDate(Integer.parseInt(properties.get(6)),Integer.parseInt(properties.get(5))-1,Integer.parseInt(properties.get(4)));
    }

    public void commitChanges(View view){
        EditText game = (EditText)findViewById(R.id.gameName);
        Spinner genre = (Spinner)findViewById(R.id.genreSpinner);
        EditText publisher   = (EditText)findViewById(R.id.publisherName);
        Spinner platform = (Spinner)findViewById(R.id.platformSpinner);
        DatePicker releaseDate = (DatePicker)findViewById(R.id.releaseDatePicker);
        int   day  = releaseDate.getDayOfMonth();
        int   month= releaseDate.getMonth();
        int   year = releaseDate.getYear();
        String formatedDate = day + "," + month + "," + year;

        String itemData = game.getText().toString() + "," +
                Integer.toString(genre.getSelectedItemPosition()) + "," +
                publisher.getText().toString() + "," +
                Integer.toString(platform.getSelectedItemPosition()) + "," +
                formatedDate.toString();

        itemsDict.put(selectedId,itemData);
        setContentView(R.layout.activity_listitems);
    }

    public void sendMail(View view){

        EditText game = (EditText)findViewById(R.id.gameName);
        Spinner genre = (Spinner)findViewById(R.id.genreSpinner);
        EditText publisher   = (EditText)findViewById(R.id.publisherName);
        Spinner platform = (Spinner)findViewById(R.id.platformSpinner);
        DatePicker releaseDate = (DatePicker)findViewById(R.id.releaseDatePicker);
        int   day  = releaseDate.getDayOfMonth();
        int   month= releaseDate.getMonth();
        int   year = releaseDate.getYear();

        String formatedDate = day + "-" + month + "-" + year;

        String mailInput = "Game Name: " + game.getText().toString() + "\n" +
                "Genre: " + genre.getSelectedItem().toString() + "\n" +
                "Publisher: " + publisher.getText().toString() + "\n" +
                "Release Date: " + formatedDate.toString() + "\n" +
                "Platform: " + platform.getSelectedItem().toString();


        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"imapnah@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Input Form Details");
        i.putExtra(Intent.EXTRA_TEXT   , mailInput);
        try {
            startActivity(Intent.createChooser(i, "Send mail using..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
