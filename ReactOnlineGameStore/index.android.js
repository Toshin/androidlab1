/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 import React, { Component } from 'react';
 import {
   AppRegistry,
   StyleSheet,
   Text,
   View,
   ListView,
   ScrollView,
   Image,
   TextInput,
   Navigator,
   TouchableOpacity,
   Alert,
   TouchableHighlight,
 } from 'react-native';

 import Communications from 'react-native-communications';

// list of items
 var games = [
     { "name": "Battlefield 3", "genre": "FPS", "price" : "100"},
     { "name": "Battlefield 4", "genre": "FPS", "price" : "200"},
     { "name": "World of Warcraft", "genre": "MMORPG" , "price" : "500"},
     { "name": "World of Tanks", "genre": "MMO", "price" : "0"},
     { "name": "Counter-Strike GO", "genre": "FPS", "price" : "100"}
   ]
 import Button from 'react-native-button';

 var SCREEN_WIDTH = require('Dimensions').get('window').width;

 var BaseConfig = Navigator.SceneConfigs.FloatFromRight;

 var CustomLeftToRightGesture = Object.assign({}, BaseConfig.gestures.pop, {
   // Make it snap back really quickly after canceling pop
   snapVelocity: 8,
   // Make it so we can drag anywhere on the screen
   edgeHitWidth: SCREEN_WIDTH,
 });

 var CustomSceneConfig = Object.assign({}, BaseConfig, {
   // A very tighly wound spring will make this transition fast
   springTension: 100,
   springFriction: 1,
   // Use our custom gesture defined above
   gestures: {
     pop: CustomLeftToRightGesture,
   }
 });


 class MyScene extends React.Component {

 	  constructor(props) {
     super(props);
     this.state = {

     };
   }

 	_handlePress() {

 		games.push({ "name": this.state.name, "genre": this.state.genre, "price" : this.state.price});

 		Communications.email(["imapnah@gmail.com"],"","","OGS - Your Order",
                         "You have ordered the following game: \n" +
                         "Game name: " + this.state.name +
                         "\nGame genre: " + this.state.genre +
                         "\nGame price: " + this.state.price +
                         "\n \nBest regards, \n"+
                         "OGS - Online Game Store.");

 		this.props.navigator.pop();
   }

   render() {
     return (
     <View style={{padding: 10}}>
         <TextInput
 			style={{height: 60}}
 			placeholder="NAME"
 		    ref= "name"
 			onChangeText={(name) => this.setState({name})}
 			value={this.state.name}
         />
 		 <TextInput
 			style={{height: 60}}
 			placeholder="GENRE"
 			ref= "genre"
 			onChangeText={(genre) => this.setState({genre})}
 			value={this.state.genre}
         />
 		 <TextInput
 			style={{height: 60}}
 			placeholder="PRICE"
 			ref= "price"
 			onChangeText={(price) => this.setState({price})}
 			value={this.state.price}
         />

 		 <Button
 		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
 			style={{fontSize: 20, color: 'white'}}
 			styleDisabled={{color: 'red'}}
 			onPress={() => this._handlePress()}>
 			Add Game
 		</Button>

       </View>
     )
   }
 }



 class EditScene extends React.Component {

 	constructor(props) {
     super(props);
     this.state = {
 			name : props.game.name,
 			genre : props.game.genre,
 			price : props.game.price,
     };
   }

   //get data
 	_handlePress() {


 		console.log(this.props.update);

    // do stuff in edit scene
 		this.props.game.name = this.state.name;
 		this.props.game.genre = this.state.genre;
 		this.props.game.price = this.state.price;
 		this.props.update;
    // go back to main scene
 		this.props.navigator.pop();
   }

   render() {

     return (
     <View style={{padding: 10}}>
         <TextInput
 			style={{height: 60}}
 		    ref= "name"
 			onChangeText={(name) => this.setState({name})}
 			value={this.state.name}
         />
 		 <TextInput
 			style={{height: 60}}
 			ref= "genre"
 			onChangeText={(genre) => this.setState({genre})}
 			value={this.state.genre}
         />
 		 <TextInput
 			style={{height: 60}}
 			ref= "price"
 			onChangeText={(price) => this.setState({price})}
 			value={this.state.price}
         />

 		 <Button
 		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
 			style={{fontSize: 20, color: 'white'}}
 			styleDisabled={{color: 'red'}}
 			onPress={() => this._handlePress()}>
 			Edit Game
 		</Button>

       </View>
     )
   }
 }


 class ReactOnlineGameStore extends Component {

 	constructor(props){
 		super(props);

 		this.state={
 			dataSource: new ListView.DataSource({
 			rowHasChanged: (row1, row2) => row1 !== row2,
 		      }),
 		      loaded: false,
 		}
 	}


  //id 3 = editscene
 	_onPressButton(game) {
 		this.props.navigator.push({id: 3,
 			passProps:{
 				game : game,
 				update : this.updateList.bind(this)
 			}
 		});


 	}

  //set data source (the games list of objects)
 	componentDidMount(){
 	this.setState({
 		  dataSource: this.state.dataSource.cloneWithRows(games),
 		  loaded: true,
 		});
 	}

  //to be implemented - update doesn't work yet
 	updateList(){
 		this.setState({
 		  dataSource: this.state.dataSource.cloneWithRows(games),
 		  loaded: true,
 		});
 	}


 // 	setThings(){
 // 		this.setState({
 // 		  loaded: false
 // 		});
 // 	}

 	renderGame(game){

 		return (
 		<TouchableOpacity onPress={this._onPressButton.bind(this,game)}>
 			<View
 			style={styles.viewDetails}>
 				<Text>{game.name}</Text>
 				<Text>{game.genre}</Text>
 				<Text>{game.price}</Text>
 			</View>
 		</TouchableOpacity>
 			);
 	}


  render(){
 	 if (!this.state.loaded){
 		 return (<Text> Loading... </Text>);
 	 }

 	 return(
 		<ListView
 			dataSource={this.state.dataSource}
 			renderRow={this.renderGame.bind(this)}
 			style={styles.listView}
 		/>
 	 );

 	}

 }


 class App extends Component {

 	constructor(props){
 		super(props);
 	}

 	_handlePress() {
 		this.props.navigator.push({id: 2});
 	}

  //calls the ReactOnlineGameStore component and lists the games
   render() {
     return (

       <View style={styles.container}>
         <Text style={styles.welcome}>
           Online Game Store
         </Text>
 		 <Button
 		 containerStyle={{padding:10, height:45, overflow:'hidden', borderWidth:1, borderStyle:'solid', borderRadius:4, backgroundColor: 'orange'}}
 			style={{fontSize: 20, color: 'white'}}
 			styleDisabled={{color: 'red'}}
 			onPress={() => this._handlePress()}>
 			Add new Game
 		</Button>
 		<ReactOnlineGameStore navigator={this.props.navigator} />
       </View>
     );
   }
 }

 //main app that fills the navigator with the scenes
 var MainApp = React.createClass({
   _renderScene(route, navigator) {
     if (route.id === 1) {
       return <App navigator={navigator} />
     } else if (route.id === 2) {
       return <MyScene navigator={navigator} />
     } else if (route.id === 3){
 		return <EditScene navigator={navigator} {...route.passProps} />
 	}
   },
   _configureScene(route) {
     return CustomSceneConfig;
   },



   render() {
     return (
       <Navigator
         initialRoute={{id: 1}}
         renderScene={this._renderScene}
         configureScene={this._configureScene} />
     );
   }
 });

 const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: '#F5FCFF',
   },
   welcome: {
     fontSize: 20,
     textAlign: 'center',
     margin: 10,
   },
 listView: {
 	width: 300,
     paddingTop: 20,
     backgroundColor: '#F5FCFF',
   },
   instructions: {
     textAlign: 'center',
     color: '#333333',
     marginBottom: 5,
   },
   viewDetails: {
 	  margin: 5
   }
 });

 AppRegistry.registerComponent('ReactOnlineGameStore', () => MainApp);
