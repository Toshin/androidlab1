package com.example.toshin.onlinegamestore;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import com.example.toshin.onlinegamestore.dummy.DummyContent;
import com.example.toshin.onlinegamestore.R;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;

public class AddActivity extends AppCompatActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Button saveButton = (Button)findViewById(R.id.save);
        saveButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                final EditText i = (EditText)findViewById(R.id.editid);
                String id = i.getText().toString();

                final EditText t = (EditText)findViewById(R.id.edittitle);
                String title = t.getText().toString();

                final EditText d = (EditText)findViewById(R.id.editdescription);
                String details = d.getText().toString();

                final EditText p = (EditText)findViewById(R.id.editprice);
                int price = Integer.parseInt(p.getText().toString());

                final EditText actiont = (EditText)findViewById(R.id.editActionPrecentage);
                int action = Integer.parseInt(actiont.getText().toString());

                final EditText simulatort = (EditText)findViewById(R.id.editSimulatorPrecentage);
                int simulator = Integer.parseInt(simulatort.getText().toString());

                final EditText shootert = (EditText)findViewById(R.id.editShooterPrecentage);
                int shooter = Integer.parseInt(shootert.getText().toString());

                final EditText rpgt = (EditText)findViewById(R.id.editRPGPrecentage);
                int rpg = Integer.parseInt(rpgt.getText().toString());

                switch (v.getId()) {
                    case  R.id.save: {

                        DummyContent.Game g = new DummyContent.Game(id,title,details,price,action,simulator,shooter,rpg);
                        DummyContent.addItem(g);



                        String email = "Hello. You have ordered the following game:" + "\n" +
                                "Game Id: "+ id + "\n" +
                                "Game Title: "+ title + "\n" +
                                "Game Description: "+ details + "\n" +
                                "Game Price: "+ price + "\n" +
                                "Best regards, \n OGS - Online Game Store";

                        Intent sendIntent = new Intent(Intent.ACTION_SEND);
                        sendIntent.setType("message/rfc822");
                        sendIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"marianlaslo@yahoo.com"});
                        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "OGS - Your Order");
                        sendIntent.putExtra(Intent.EXTRA_TEXT   , email+"\n"+"\n"+"\n"+"\n"+"\nSent from "+android.os.Build.MODEL);
                        try {
                            startActivity(sendIntent);
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(AddActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }


                        try {
                            createFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Toast toast = Toast.makeText(AddActivity.this,"Game has been added to the database. An email has been sent to confirm it.", Toast.LENGTH_SHORT);
                        toast.show();



                    }
                }
            }


            public void createFile() throws IOException, JSONException {

                JSONArray data = new JSONArray();
                JSONObject game;


                for(int i =0 ; i< DummyContent.ITEMS.size(); i++){
                    game = new JSONObject();
                    game.put("id",DummyContent.ITEMS.get(i).id);
                    game.put("title",DummyContent.ITEMS.get(i).title);
                    game.put("description",DummyContent.ITEMS.get(i).description);
                    game.put("price",DummyContent.ITEMS.get(i).price);
                    game.put("action",DummyContent.ITEMS.get(i).action);
                    game.put("simulator",DummyContent.ITEMS.get(i).simulator);
                    game.put("shooter",DummyContent.ITEMS.get(i).shooter);
                    game.put("rpg",DummyContent.ITEMS.get(i).rpg);
                    data.put(game);
                }

                String text = data.toString();

                FileOutputStream fos = openFileOutput("gamesFile", MODE_PRIVATE);
                fos.write(text.getBytes());
                fos.close();

                Toast toast = Toast.makeText(AddActivity.this,"Data saved", Toast.LENGTH_SHORT);
                toast.show();

            }


        });




    }




}
