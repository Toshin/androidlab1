package com.example.toshin.onlinegamestore;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;
import com.example.toshin.onlinegamestore.R;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChartSecondActivity extends AppCompatActivity {


    private static String TAG = "Chartactivity";

    public ArrayList<Integer> genres2 = new ArrayList<Integer>();

    List<Integer> yDataList = new ArrayList<>(Arrays.asList(0,0,0,0));

    List<String> xDataList = new ArrayList<>(Arrays.asList("","","",""));

    PieChart pieChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_second);

        genres2 = getIntent().getIntegerArrayListExtra("genresValues");

        HashMap<String,Integer> dictionary = new HashMap<String, Integer>();

        dictionary.put("Action",genres2.get(0));
        dictionary.put("Simulator",genres2.get(1));
        dictionary.put("Shooter",genres2.get(2));
        dictionary.put("Rpg",genres2.get(3));


        for(String key : dictionary.keySet()){
            dictionary.put(key,dictionary.get(key));
        }

        System.out.println(dictionary);

            yDataList.set(0,dictionary.get("Action"));
            xDataList.set(0,"Action");

        yDataList.set(1,dictionary.get("Simulator"));
        xDataList.set(1,"Simulator");

        yDataList.set(2,dictionary.get("Shooter"));
        xDataList.set(2,"Shooter");

        yDataList.set(3,dictionary.get("Rpg"));
        xDataList.set(3,"Rpg");


        pieChart = (PieChart) findViewById(R.id.chart1);



        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Game details");
        pieChart.setCenterTextSize(10);
        Description d  =new Description();
        d.setText("Each slice is a game genre");
        d.setTextColor(Color.RED);
        d.setTextSize(10);
        pieChart.setDescription(d);
        //pieChart.setDrawEntryLabels(true);

        addDataSet();


        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

                String brand = xDataList.get((int)h.getX());
                Toast.makeText(ChartSecondActivity.this,  brand  ,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });


    }


    private void addDataSet() {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();

        ArrayList<PieEntry> asd = new ArrayList<>();


        for(int i = 0 ; i< yDataList.size() ; i++){
            yEntrys.add(new PieEntry(yDataList.get(i),i));
        }

        for(int i = 0 ; i< xDataList.size() ; i++){
            xEntrys.add(xDataList.get(i));
        }

        // create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "Game genres");
        pieDataSet.setSliceSpace(10);
        pieDataSet.setValueTextSize(20);
        pieDataSet.setSelectionShift(5);


        // add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.RED);
        colors.add(Color.BLUE);
        colors.add(Color.GREEN);
        colors.add(Color.CYAN);
        colors.add(Color.GRAY);
        colors.add(Color.YELLOW);
        colors.add(Color.MAGENTA);

        pieDataSet.setColors(colors);

        //add legend to chart
        Legend legend = pieChart.getLegend();

        legend.setForm(Legend.LegendForm.SQUARE);
        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);
        legend.setXEntrySpace(20);
        legend.setYEntrySpace(5);
        legend.setTextSize(14);
        legend.setWordWrapEnabled(true);


        // create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);

        // update pieChart
        pieChart.invalidate();
    }
}
